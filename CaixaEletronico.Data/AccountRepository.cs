﻿using CaixaEletronico.Core;

namespace CaixaEletronico.Data
{
    public class AccountRepository : DataRepository<Account>
    {
        public AccountRepository()
            : base(DatabaseFactory.GetDb())
        {
        }

        public override void Insert(Account item)
        {
            base.Insert(item);
        }
    }
}
