﻿using CaixaEletronico.Core;

namespace CaixaEletronico.Data
{
    public class ClientRepository : DataRepository<Client>
    {
        public ClientRepository()
            : base(DatabaseFactory.GetDb())
        {
        }
    }
}
