﻿using CaixaEletronico.Core;
using CaixaEletronico.Data.Databases;
using System.Collections.Generic;

namespace CaixaEletronico.Data
{
    public abstract class DataRepository<T> : IRepository<T>
        where T : IEntity
    {
        private IDatabase _database;

        public DataRepository(IDatabase database)
        {
            this._database = database;
        }

        public virtual void Delete(T item)
        {
            _database.Delete(item);
        }

        public virtual List<T> Get()
        {
            return _database.Get<T>();
        }

        public virtual void Insert(T item)
        {
            _database.Insert(item);
        }

        public virtual void Update(T item)
        {
            _database.Update(item);
        }

        public virtual void Clear()
        {
            _database.Clear();
        }
    }
}
