﻿using CaixaEletronico.Data.Databases;

namespace CaixaEletronico.Data
{
    public class DatabaseFactory
    {
        public static bool IsTest;

        internal static IDatabase GetDb()
        {
            return InMemoryDB.Instance;

            //if (IsTest)
            //    return InMemoryDB.Instance;
            //else
            //    return new LiteDatabaseWrapper();
        }
    }
}