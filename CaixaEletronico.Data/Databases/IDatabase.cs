﻿using CaixaEletronico.Core;
using System.Collections.Generic;

namespace CaixaEletronico.Data.Databases
{
    public interface IDatabase
    {
        void Insert<T>(T item) where T : IEntity;

        void Update<T>(T item) where T : IEntity;

        void Delete<T>(T item) where T : IEntity;

        List<T> Get<T>() where T : IEntity;

        void Clear();
    }
}
