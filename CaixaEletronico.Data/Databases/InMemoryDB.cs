﻿using CaixaEletronico.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CaixaEletronico.Data.Databases
{
    internal class InMemoryDB : IDatabase
    {
        private static Dictionary<Type, IList<object>> _items = new Dictionary<Type, IList<object>>();

        private static InMemoryDB _instance;

        private InMemoryDB() { }

        public static InMemoryDB Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new InMemoryDB();

                return _instance;
            }
        }

        public void Insert<T>(T item)
            where T : IEntity
        {
            if (!_items.ContainsKey(typeof(T)))
                _items.Add(typeof(T), new List<object>());

            if (_items[typeof(T)].Any())
                item.Id = _items[typeof(T)].Cast<T>().Max(i => i.Id) + 1;
            _items[typeof(T)].Add(item);
        }

        public void Update<T>(T item) where T : IEntity
        {
            Delete(item);
            Insert(item);
        }

        public void Delete<T>(T item) where T : IEntity
        {
            _items[typeof(T)].Remove(item);
        }

        public List<T> Get<T>() where T : IEntity
        {
            if (!_items.ContainsKey(typeof(T)))
                _items.Add(typeof(T), new List<object>());

            return _items[typeof(T)].Cast<T>().ToList();
        }

        public void Clear()
        {
            _items.Clear();
        }
    }
}
