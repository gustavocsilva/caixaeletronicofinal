﻿using CaixaEletronico.Core;
using System.Collections.Generic;

namespace CaixaEletronico.Data
{
    public interface IRepository<T>
        where T : IEntity
    {
        void Insert(T item);

        void Update(T item);

        void Delete(T item);

        List<T> Get();

        void Clear();
    }
}