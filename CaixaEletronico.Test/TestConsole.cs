﻿using System.Collections.Generic;
using System.Linq;
using CaixaEletronico.IO;

namespace CaixaEletronico.Test
{
    public class TestConsole : BaseConsole
    {
        private IList<string> _linesToRead;
        private int index = 0;

        public TestConsole(IList<string> linesToRead)
        {
            this._linesToRead = linesToRead;
        }

        public override string ReadLine()
        {
            if (index >= _linesToRead.Count)
                return null;

            string line = _linesToRead[index];
            index++;
            return line;
        }

        protected override void Write(string message)
        {
        }
    }
}