using CaixaEletronico.Core;
using CaixaEletronico.Data;
using CaixaEletronico.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace CaixaEletronico.Test
{
    [TestClass]
    public class UnitTest1
    {
        public ClientRepository ClientRepository { get; set; }

        public AccountRepository AccountRepository { get; set; }

        public UnitTest1()
        {
            DatabaseFactory.IsTest = true;

            ClientRepository = new ClientRepository();
            ClientRepository.Clear();

            AccountRepository = new AccountRepository();
            AccountRepository.Clear();
        }

        [TestMethod]
        public void Home()
        {
            TestConsole testConsole = new TestConsole(new List<string> { "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            Assert.AreEqual(4, testConsole.LinesWriten.Count);
            AssertHome(testConsole.LinesWriten, 0);
        }

        [TestMethod]
        public void Subscribe()
        {
            TestConsole testConsole = new TestConsole(new List<string> { "2", "Gustavo", "senha", "111", "F", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Subscribe", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);
            Assert.AreEqual("Digite o documento:", testConsole.LinesWriten[7]);
            Assert.AreEqual("Digite o tipo do cliente(P ou F):", testConsole.LinesWriten[8]);

            List<Client> clients = ClientRepository.Get();
            Assert.AreEqual(clients.Count(), 1);
            Assert.AreEqual("Gustavo", clients.Single().Username);
            Assert.AreEqual("senha", clients.Single().Password);
            Assert.AreEqual("111", clients.Single().Document);
            Assert.AreEqual(ClientType.PF, clients.Single().Type);
        }

        [TestMethod]
        public void SubscribeError()
        {
            TestConsole testConsole = new TestConsole(new List<string> { "2", "Gustavo", "senha", "111", "F", "2", "Gustavo", "senha", "111", "F", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Subscribe", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);
            Assert.AreEqual("Digite o documento:", testConsole.LinesWriten[7]);
            Assert.AreEqual("Digite o tipo do cliente(P ou F):", testConsole.LinesWriten[8]);

            index = 10;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Subscribe", testConsole.LinesWriten[14]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[15]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[16]);
            Assert.AreEqual("Digite o documento:", testConsole.LinesWriten[17]);
            Assert.AreEqual("Digite o tipo do cliente(P ou F):", testConsole.LinesWriten[18]);
            Assert.AreEqual("Usu�rio n�o pode ser cadastrado", testConsole.LinesWriten[19]);

            List<Client> clients = ClientRepository.Get();
            Assert.AreEqual(clients.Count(), 1);
            Assert.AreEqual("Gustavo", clients.Single().Username);
            Assert.AreEqual("senha", clients.Single().Password);
            Assert.AreEqual("111", clients.Single().Document);
            Assert.AreEqual(ClientType.PF, clients.Single().Type);
        }


        [TestMethod]
        public void Login()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senha", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);
        }

        [TestMethod]
        public void Login_Invalido()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senhaerrada", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Usu�rio inv�lido", testConsole.LinesWriten[7]);

            AssertHome(testConsole.LinesWriten, 8);
        }

        [TestMethod]
        public void Balance()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            var client = ClientRepository.Get().Single();
            AccountRepository.Insert(new Account { Balance = 934.45M, Owner = client });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senha", "1", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);

            Assert.AreEqual("Saldo: R$934,45", testConsole.LinesWriten[13]);
        }

        [TestMethod]
        public void Deposit()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            var client = ClientRepository.Get().Single();
            AccountRepository.Insert(new Account { Balance = 934.45M, Owner = client });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senha", "2", "10", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);

            Assert.AreEqual("Digite um valor", testConsole.LinesWriten[13]);
            Assert.AreEqual("Dep�sito realizado com sucesso", testConsole.LinesWriten[14]);
            var account = AccountRepository.Get().Single();
            Assert.AreEqual(944.45M, account.Balance);
        }

        [TestMethod]
        public void DepositInvalidValue()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            var client = ClientRepository.Get().Single();
            AccountRepository.Insert(new Account { Balance = 934.45M, Owner = client });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senha", "2", "abcd", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);

            Assert.AreEqual("Digite um valor", testConsole.LinesWriten[13]);
            Assert.AreEqual("Valor Inv�lido", testConsole.LinesWriten[14]);
        }

        [TestMethod]
        public void Withdraw()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            var client = ClientRepository.Get().Single();
            AccountRepository.Insert(new Account { Balance = 934.45M, Owner = client });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senha", "3", "10", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);

            Assert.AreEqual("Digite um valor", testConsole.LinesWriten[13]);
            Assert.AreEqual("Saque realizado com sucesso", testConsole.LinesWriten[14]);
            var account = AccountRepository.Get().Single();
            Assert.AreEqual(924.45M, account.Balance);
        }

        [TestMethod]
        public void WithdrawInvalidValue()
        {
            ClientRepository.Insert(new Client { Username = "Gustavo", Password = "senha", Document = "123", Type = ClientType.PF });
            var client = ClientRepository.Get().Single();
            AccountRepository.Insert(new Account { Balance = 934.45M, Owner = client });
            TestConsole testConsole = new TestConsole(new List<string> { "1", "Gustavo", "senha", "3", "abcd", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);

            Assert.AreEqual("Digite um valor", testConsole.LinesWriten[13]);
            Assert.AreEqual("Valor Inv�lido", testConsole.LinesWriten[14]);
        }

        [TestMethod]
        public void Transfer()
        {
            ClientRepository.Insert(new Client { Username = "1", Password = "senha", Document = "123", Type = ClientType.PF });
            var client = ClientRepository.Get().Single();
            AccountRepository.Insert(new Account { Balance = 100, Owner = client });
            var account = AccountRepository.Get().Single();

            ClientRepository.Insert(new Client { Username = "2", Password = "senha", Document = "123", Type = ClientType.PF });
            var client2 = ClientRepository.Get().Single(c => c.Username == "2");
            AccountRepository.Insert(new Account { Balance = 200, Owner = client2 });

            TestConsole testConsole = new TestConsole(new List<string> { "1", "2", "senha", "4", account.Number.ToString(), "100", "Q", "Q" });
            ConsoleManager.Console = testConsole;

            Program.Main(null);
            int index = 0;
            index = AssertHome(testConsole.LinesWriten, index);

            Assert.AreEqual("Login", testConsole.LinesWriten[4]);
            Assert.AreEqual("Digite um nome:", testConsole.LinesWriten[5]);
            Assert.AreEqual("Digite uma senha:", testConsole.LinesWriten[6]);

            Assert.AreEqual("Home Cliente", testConsole.LinesWriten[7]);
            Assert.AreEqual("1 - Saldo", testConsole.LinesWriten[8]);
            Assert.AreEqual("2 - Deposito", testConsole.LinesWriten[9]);
            Assert.AreEqual("3 - Saque", testConsole.LinesWriten[10]);
            Assert.AreEqual("4 - Transferencia", testConsole.LinesWriten[11]);
            Assert.AreEqual("Q - Sair", testConsole.LinesWriten[12]);

            Assert.AreEqual("Digite a conta destino", testConsole.LinesWriten[13]);
            Assert.AreEqual("Digite um valor", testConsole.LinesWriten[14]);
            Assert.AreEqual("Transfer�ncia realizada com sucesso", testConsole.LinesWriten[15]);
            var accounts = AccountRepository.Get();

            Assert.AreEqual(100, accounts.First().Balance);
            Assert.AreEqual(100, accounts.First().Balance);
        }

        private int AssertHome(List<string> lines, int startIndex)
        {
            Assert.AreEqual("Bem vindo ao Banco", lines[startIndex]);
            Assert.AreEqual("1 - Login", lines[startIndex + 1]);
            Assert.AreEqual("2 - Cadastrar", lines[startIndex + 2]);
            Assert.AreEqual("Q - Sair", lines[startIndex + 3]);
            return startIndex + 3;
        }
    }
}
