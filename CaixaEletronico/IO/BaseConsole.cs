﻿using System;
using System.Collections.Generic;

namespace CaixaEletronico.IO
{
    public abstract class BaseConsole
    {
        public BaseConsole()
        {
            this.LinesWriten = new List<string>();
        }

        public List<String> LinesWriten { get; set; }

        protected abstract void Write(string message);

        public void WriteLine(string message)
        {
            this.LinesWriten.Add(message);
            this.Write(message);
        }

        public abstract string ReadLine();
    }
}