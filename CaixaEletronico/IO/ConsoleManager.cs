﻿namespace CaixaEletronico.IO
{
    public static class ConsoleManager
    {
        private static BaseConsole _console;

        public static BaseConsole Console
        {
            set
            {
                _console = value;
            }
            get
            {
                return _console ?? ConsoleWrapper.Instance;
            }
        }
    }
}
