﻿using System;

namespace CaixaEletronico.IO
{
    public class ConsoleWrapper : BaseConsole
    {
        private static ConsoleWrapper _console;

        private ConsoleWrapper()
        {

        }

        public static ConsoleWrapper Instance
        {
            get
            {
                if (_console == null)
                    _console = new ConsoleWrapper();

                return _console;
            }
        }

        protected override void Write(string message)
        {
            Console.WriteLine(message);
        }

        public override string ReadLine()
        {
            return Console.ReadLine();
        }
    }
}
