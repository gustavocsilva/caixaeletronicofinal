﻿using CaixaEletronico.Display;
using CaixaEletronico.IO;

namespace CaixaEletronico
{
    public class Program
    {

        public static void Main(string[] args)
        {
            WindowManager.Render(new HomeScreen());
        }
    }
}
