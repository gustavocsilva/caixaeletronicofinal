﻿using CaixaEletronico.IO;

namespace CaixaEletronico.Display
{
    public class HomeScreen : IScreen
    {
        public void Run()
        {
            ConsoleManager.Console.WriteLine("Bem vindo ao Banco");
            ConsoleManager.Console.WriteLine("1 - Login");
            ConsoleManager.Console.WriteLine("2 - Cadastrar");
            ConsoleManager.Console.WriteLine("Q - Sair");

            string key = ConsoleManager.Console.ReadLine();

            switch (key.ToLower())
            {
                case "1":
                    WindowManager.Render(new LoginScreen());
                    break;
                case "2":
                    WindowManager.Render(new SubscribeScreen());
                    break;
                case "q":
                    break;
                default:
                    ConsoleManager.Console.WriteLine("Código inválido");
                    this.Run();
                    break;
            }
        }
    }
}
