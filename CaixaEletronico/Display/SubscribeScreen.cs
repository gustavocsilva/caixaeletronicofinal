﻿using CaixaEletronico.Business;
using CaixaEletronico.IO;

namespace CaixaEletronico.Display
{
    class SubscribeScreen : IScreen
    {
        public void Run()
        {
            ConsoleManager.Console.WriteLine("Subscribe");

            ConsoleManager.Console.WriteLine("Digite um nome:");
            string name = ConsoleManager.Console.ReadLine();

            ConsoleManager.Console.WriteLine("Digite uma senha:");
            string password = ConsoleManager.Console.ReadLine();

            ConsoleManager.Console.WriteLine("Digite o documento:");
            string document = ConsoleManager.Console.ReadLine();

            ConsoleManager.Console.WriteLine("Digite o tipo do cliente(P ou F):");
            string type = ConsoleManager.Console.ReadLine();

            ClientManager manager = new ClientManager();
            var client = manager.Subscribe(name, password, document, type);

            if (client == null)
                ConsoleManager.Console.WriteLine("Usuário não pode ser cadastrado");
            else
            {
                var account = new AccountManager().Get(client);
                ConsoleManager.Console.WriteLine($"Usuário cadastrado com sucesso. Conta: {account.Number}");
            }

            WindowManager.Dismiss();
        }
    }
}