﻿using System.Collections.Generic;

namespace CaixaEletronico.Display
{
    public static class WindowManager
    {
        private static Stack<IScreen> _stack = new Stack<IScreen>();

        public static void Render(IScreen window)
        {
            _stack.Push(window);
            window.Run();
        }

        public static void Replace(IScreen window)
        {
            _stack.Pop();
            Render(window);
        }

        public static void Dismiss()
        {
            _stack.Pop();
            _stack.Peek().Run();
        }
    }
}
