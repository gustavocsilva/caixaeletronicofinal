﻿using CaixaEletronico.Business;
using CaixaEletronico.Core;
using CaixaEletronico.IO;

namespace CaixaEletronico.Display
{
    public class LoginScreen : IScreen
    {
        public void Run()
        {
            ConsoleManager.Console.WriteLine("Login");

            ConsoleManager.Console.WriteLine("Digite um nome:");
            string username = ConsoleManager.Console.ReadLine();

            ConsoleManager.Console.WriteLine("Digite uma senha:");
            string password = ConsoleManager.Console.ReadLine();

            ClientManager manager = new ClientManager();
            Client client = manager.Find(username, password);

            if (client == null)
            {
                ConsoleManager.Console.WriteLine("Usuário inválido");
                WindowManager.Dismiss();
            }
            else
                WindowManager.Replace(new ClientHomeScreen(client));
        }
    }
}
