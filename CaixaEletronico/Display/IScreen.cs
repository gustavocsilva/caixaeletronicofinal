﻿namespace CaixaEletronico.Display
{
    public interface IScreen
    {
        void Run();
    }
}
