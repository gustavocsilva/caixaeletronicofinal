﻿using CaixaEletronico.Business;
using CaixaEletronico.Core;
using CaixaEletronico.IO;
using System;

namespace CaixaEletronico.Display
{
    internal class ClientHomeScreen : IScreen
    {
        private Account account;

        public ClientHomeScreen(Client client)
        {
            this.account = new AccountManager().Get(client);
        }

        public void Run()
        {
            ConsoleManager.Console.WriteLine("Home Cliente");
            ConsoleManager.Console.WriteLine("1 - Saldo");
            ConsoleManager.Console.WriteLine("2 - Deposito");
            ConsoleManager.Console.WriteLine("3 - Saque");
            ConsoleManager.Console.WriteLine("4 - Transferencia");
            ConsoleManager.Console.WriteLine("Q - Sair");

            string key = ConsoleManager.Console.ReadLine();
            switch (key.ToLower())
            {
                case "1":
                    ConsoleManager.Console.WriteLine($"Saldo: {account.Balance.ToString("C")}");
                    this.Run();
                    break;
                case "2":
                    Deposit();
                    this.Run();
                    break;
                case "3":
                    Withdraw();
                    this.Run();
                    break;
                case "4":
                    Transfer();
                    this.Run();
                    break;
                case "q":
                    WindowManager.Dismiss();
                    break;
                default:
                    break;
            }
        }

        private void Deposit()
        {
            ConsoleManager.Console.WriteLine("Digite um valor");
            bool valid = decimal.TryParse(ConsoleManager.Console.ReadLine(), out decimal value);
            if (!valid)
                ConsoleManager.Console.WriteLine("Valor Inválido");
            else
            {
                new AccountManager().Deposit(account, value);
                ConsoleManager.Console.WriteLine("Depósito realizado com sucesso");
            }
        }

        private void Withdraw()
        {
            ConsoleManager.Console.WriteLine("Digite um valor");
            bool valid = decimal.TryParse(ConsoleManager.Console.ReadLine(), out decimal value);
            if (!valid)
                ConsoleManager.Console.WriteLine("Valor Inválido");
            else
            {
                new AccountManager().Withdraw(account, value);
                ConsoleManager.Console.WriteLine("Saque realizado com sucesso");
            }
        }

        private void Transfer()
        {
            ConsoleManager.Console.WriteLine("Digite a conta destino");
            var accountNumber = Convert.ToInt32(ConsoleManager.Console.ReadLine());
            var manager = new AccountManager();
            var destination = manager.Get(accountNumber);

            ConsoleManager.Console.WriteLine("Digite um valor");
            bool valid = decimal.TryParse(ConsoleManager.Console.ReadLine(), out decimal value);
            if (!valid)
                ConsoleManager.Console.WriteLine("Valor Inválido");
            else
            {
                new AccountManager().Transfer(account, destination, value);
                ConsoleManager.Console.WriteLine("Transferência realizada com sucesso");
            }
        }
    }
}