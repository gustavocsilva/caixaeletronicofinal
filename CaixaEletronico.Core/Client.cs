﻿namespace CaixaEletronico.Core
{
    public class Client : IEntity
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }

        public string Document { get; set; }

        public ClientType Type { get; set; }
    }
}