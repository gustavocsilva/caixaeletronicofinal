﻿using System;

namespace CaixaEletronico.Core
{
    public class Account : IEntity
    {
        public Account()
        {
            this.Number = new Random().Next(1000, 9999);
        }

        public int Id { get; set; }

        public int Number { get; protected set; }

        public Client Owner { get; set; }

        public decimal Balance { get; set; }
    }
}