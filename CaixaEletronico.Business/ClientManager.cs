﻿using CaixaEletronico.Core;
using CaixaEletronico.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CaixaEletronico.Business
{
    public class ClientManager
    {
        public List<Client> List()
        {
            return new ClientRepository().Get();
        }

        public Client Subscribe(string username, string password, string document, string type)
        {
            var client = new Client
            {
                Username = username,
                Password = password,
                Document = document,
                Type = type == "F" ? ClientType.PF : ClientType.PJ
            };

            var repo = new ClientRepository();

            if (repo.Get().Any(c => c.Username == username))
                return null;

            repo.Insert(client);

            var account = new Account { Owner = client };
            new AccountRepository().Insert(account);

            return client;
        }

        public Client Find(string username, string password)
        {
            return new ClientRepository().Get().SingleOrDefault(c => c.Username == username && c.Password == password);
        }
    }
}
