﻿using CaixaEletronico.Core;
using CaixaEletronico.Data;
using System.Linq;

namespace CaixaEletronico.Business
{
    public class AccountManager
    {
        public Account Get(Client client)
        {
            var repo = new AccountRepository();
            return repo.Get().SingleOrDefault(a => a.Owner == client);
        }

        public Account Get(int number)
        {
            var repo = new AccountRepository();
            return repo.Get().SingleOrDefault(a => a.Number == number);
        }

        public void Deposit(Account account, decimal value)
        {
            account.Balance += value;
            new AccountRepository().Update(account);
        }

        public void Withdraw(Account account, decimal value)
        {
            account.Balance -= value;
            new AccountRepository().Update(account);
        }

        public void Transfer(Account origin, Account destination, decimal value)
        {
            this.Withdraw(origin, value);
            this.Deposit(destination, value);
        }
    }
}
